/*****************************************************************//**
 * @file   main.cpp
 * @brief  The entry file and main function
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#include "pch.h"
#include "application.h"

int main(void)
{
	auto app = std::make_unique<Application>("PBR IBL Render");
	app->loop();

	return 0;
}