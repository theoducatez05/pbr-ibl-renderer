/*********************************************************************
 * @file   model.h
 * @brief  3D Model abstraction
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include "pch.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <stb_image/stb_image.h>

#include "elements/mesh.h"
#include "shader/shader.h"
#include "material/material.h"

namespace nelements
{
    class Model
    {
    public:
        // model data
        std::vector<Mesh>    mMeshes;
        nmaterial::Material  mMaterial;
        std::string mDirectory;
        bool mGammaCorrection;

        // constructor, expects a filepath to a 3D model.
        Model(std::string const& path, bool gamma = false) : mGammaCorrection(gamma)
        {
            loadModel(path);
        }

        Model()
        {

        }

        ~Model()
        {
            for (auto mesh : mMeshes)
            {
                glDeleteVertexArrays(1, &mesh.mVao);
                glDeleteBuffers(1, &mesh.mVbo);
                glDeleteBuffers(1, &mesh.mEbo);
            }
        }

        // draws the model, and thus all its meshes
        void draw(nshader::Shader& shader);

        void activateTextures(nshader::Shader& shader);

    private:
        // loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
        void loadModel(std::string const& path);

        // processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
        void processNode(aiNode* node, const aiScene* scene);

        Mesh processMesh(aiMesh* mesh, const aiScene* scene);

        void loadMaterialTextures(const aiScene* scene, aiMaterial* mat);
    };
}