/*********************************************************************
 * @file   model.cpp
 * @brief  3D Model abstraction
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/


#include "model.h"

void nelements::Model::draw(nshader::Shader& shader)
{
    activateTextures(shader);

    for (unsigned int i = 0; i < mMeshes.size(); i++)
        mMeshes[i].draw(shader);
}

void nelements::Model::activateTextures(nshader::Shader& shader)
{
    shader.use();
    // Set diffuse
    shader.setInt("material.diffuse.isTexture", false);
    if (mMaterial.getDiffuseTexture() != nullptr && mMaterial.getDiffuseTexture()->activated) {
        glActiveTexture(GL_TEXTURE0); // Activate proper texture unit before binding
        shader.setInt("material.diffuse.texture", 0);
        shader.setBool("material.diffuse.isTexture", true);
        glBindTexture(GL_TEXTURE_2D, mMaterial.getDiffuseTexture()->getId());
    }

    // Set normal
    shader.setInt("material.normal.isTexture", false);
    if (mMaterial.getNormalTexture() != nullptr && mMaterial.getNormalTexture()->activated) {
        glActiveTexture(GL_TEXTURE0 + 1); // Activate proper texture unit before binding
        shader.setInt("material.normal.texture", 1);
        shader.setBool("material.normal.isTexture", true);
        glBindTexture(GL_TEXTURE_2D, mMaterial.getNormalTexture()->getId());
    }

    // Set metallic
    shader.setInt("material.metallic.isTexture", false);
    if (mMaterial.getMetallicTexture() != nullptr && mMaterial.getMetallicTexture()->activated) {
        glActiveTexture(GL_TEXTURE0 + 2); // Activate proper texture unit before binding
        shader.setInt("material.metallic.texture", 2);
        shader.setBool("material.metallic.isTexture", true);
        glBindTexture(GL_TEXTURE_2D, mMaterial.getMetallicTexture()->getId());
    }

    // Set roughness
    shader.setInt("material.roughness.isTexture", false);
    if (mMaterial.getRoughnessTexture() != nullptr && mMaterial.getRoughnessTexture()->activated) {
        glActiveTexture(GL_TEXTURE0 + 3); // Activate proper texture unit before binding
        shader.setInt("material.roughness.texture", 3);
        shader.setBool("material.roughness.isTexture", true);
        glBindTexture(GL_TEXTURE_2D, mMaterial.getRoughnessTexture()->getId());
    }

    // Set ao
    shader.setInt("material.ao.isTexture", false);
    if (mMaterial.getAmbienOcclusionTexture() != nullptr && mMaterial.getAmbienOcclusionTexture()->activated) {
        glActiveTexture(GL_TEXTURE0 + 4); // Activate proper texture unit before binding
        shader.setInt("material.ao.texture", 4);
        shader.setBool("material.ao.isTexture", true);
        glBindTexture(GL_TEXTURE_2D, mMaterial.getAmbienOcclusionTexture()->getId());
    }

    // Set height
    shader.setInt("material.height.isTexture", false);
    if (mMaterial.getHeightTexture() != nullptr && mMaterial.getHeightTexture()->activated) {
        glActiveTexture(GL_TEXTURE0 + 5); // Activate proper texture unit before binding
        shader.setInt("material.height.texture", 5);
        shader.setBool("material.height.isTexture", true);
        glBindTexture(GL_TEXTURE_2D, mMaterial.getHeightTexture()->getId());
    }
}


void nelements::Model::loadModel(std::string const& path)
{
    // read file via ASSIMP
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate |
        aiProcess_FlipUVs |
        aiProcess_GenNormals |
        aiProcess_GenUVCoords |
        aiProcess_LimitBoneWeights |
        aiProcess_CalcTangentSpace);
    // check for errors
    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
    {
        std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
        return;
    }
    // retrieve the directory path of the filepath
    mDirectory = path.substr(0, path.find_last_of('\\'));

    // process ASSIMP's root node recursively
    processNode(scene->mRootNode, scene);
}

void nelements::Model::processNode(aiNode* node, const aiScene* scene)
{
    // process each mesh located at the current node
    for (unsigned int i = 0; i < node->mNumMeshes; i++)
    {
        // the node object only contains indices to index the actual objects in the scene.
        // the scene contains all the data, node is just to keep stuff organized (like relations between nodes).
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        mMeshes.push_back(processMesh(mesh, scene));
    }
    // after we've processed all of the meshes (if any) we then recursively process each of the children nodes
    for (unsigned int i = 0; i < node->mNumChildren; i++)
    {
        processNode(node->mChildren[i], scene);
    }

}

nelements::Mesh nelements::Model::processMesh(aiMesh* mesh, const aiScene* scene)
{
    // data to fill
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;

    // walk through each of the mesh's vertices
    for (unsigned int i = 0; i < mesh->mNumVertices; i++)
    {
        Vertex vertex;
        glm::vec3 vector; // we declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
        // positions
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.mPosition = vector;
        // normals
        if (mesh->HasNormals())
        {
            vector.x = mesh->mNormals[i].x;
            vector.y = mesh->mNormals[i].y;
            vector.z = mesh->mNormals[i].z;
            vertex.mNormal = vector;
        }
        // texture coordinates
        if (mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
        {
            glm::vec2 vec;
            // a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
            // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
            vec.x = mesh->mTextureCoords[0][i].x;
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.mTexCoords = vec;
            // tangent
            vector.x = mesh->mTangents[i].x;
            vector.y = mesh->mTangents[i].y;
            vector.z = mesh->mTangents[i].z;
            vertex.mTangent = vector;
            // bitangent
            vector.x = mesh->mBitangents[i].x;
            vector.y = mesh->mBitangents[i].y;
            vector.z = mesh->mBitangents[i].z;
            vertex.mBitangent = vector;
        }
        else
            vertex.mTexCoords = glm::vec2(0.0f, 0.0f);

        vertices.push_back(vertex);
    }
    // now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
    for (unsigned int i = 0; i < mesh->mNumFaces; i++)
    {
        aiFace face = mesh->mFaces[i];
        // retrieve all indices of the face and store them in the indices vector
        for (unsigned int j = 0; j < face.mNumIndices; j++)
            indices.push_back(face.mIndices[j]);
    }
    // process materials
    aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
    // we assume a convention for sampler names in the shaders. Each diffuse texture should be named
    // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER.
    // Same applies to other texture as the following list summarizes:
    // diffuse: texture_diffuseN
    // specular: texture_specularN
    // normal: texture_normalN
    loadMaterialTextures(scene, material);
    // return a mesh object created from the extracted mesh data
    return Mesh(vertices, indices);
}

/**
 * @brief Loads the textures and materials
 *
 * @param scene The ASSIMP scene
 * @param mat The ASSIMP material
 * @param type The texture type
 * @param type texture type
 * @return A pointer to a texture
 */
void nelements::Model::loadMaterialTextures(const aiScene* scene, aiMaterial* mat)
{
    for (unsigned int i = 0; i < mat->GetTextureCount(aiTextureType_DIFFUSE); i++)
    {
        aiString str;
        mat->GetTexture(aiTextureType_DIFFUSE, i, &str);

        std::string filePath = std::string(str.C_Str());

        filePath = mDirectory + '/' + filePath;
        mMaterial.setTexture(filePath.c_str(), nmaterial::ETextureType::DIFFUSE, false);
    }

    for (unsigned int i = 0; i < mat->GetTextureCount(aiTextureType_NORMALS); i++)
    {
        aiString str;
        mat->GetTexture(aiTextureType_NORMALS, i, &str);

        std::string filePath = std::string(str.C_Str());

        filePath = mDirectory + '/' + filePath;
        mMaterial.setTexture(filePath.c_str(), nmaterial::ETextureType::NORMAL, false);
    }
}