/*********************************************************************
 * @file   mesh.h
 * @brief  Mesh abstraction
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include "pch.h"

#include <shader/shader.h>

namespace nelements
{
#define MAX_BONE_INFLUENCE 4

    struct Vertex
    {
        // position
        glm::vec3 mPosition;
        // normal
        glm::vec3 mNormal;
        // texCoords
        glm::vec2 mTexCoords;
        // tangent
        glm::vec3 mTangent;
        // bitangent
        glm::vec3 mBitangent;
        //bone indexes which will influence this vertex
        int m_BoneIDs[MAX_BONE_INFLUENCE];
        //weights from each bone
        float m_Weights[MAX_BONE_INFLUENCE];
    };

    class Mesh
    {
    public:
        // mesh Data
        std::vector<Vertex>         mVertices;
        std::vector<unsigned int>   mIndices;
        unsigned int mVao;
        unsigned int mVbo, mEbo;

        // constructor
        Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices);

        // render the mesh
        void draw(nshader::Shader& shader);

    private:

        // initializes all the buffer objects/arrays
        void setupMesh();
    };
}