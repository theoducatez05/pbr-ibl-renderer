/*********************************************************************
 * @file   property_panel.cpp
 * @brief  The property panel class, containing all the UI ImGUI properties
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#include "pch.h"

#include "property_panel.h"

namespace nui
{
	Property_Panel::Property_Panel()
	{
		mFileDialog.SetTitle("Load a new model");
		mFileDialog.SetFileFilters({ ".fbx", ".obj", ".dae" });

		mTextureFileDialog.SetTitle("Load a texture");
		mTextureFileDialog.SetFileFilters({ ".png", ".jpg", ".tga", ".jpeg" });
	}

	void Property_Panel::render(nscene::Scene* scene)
	{
		auto model = scene->get_model();

		ImGui::Begin("Properties panel");

		ImGui::Dummy({ 0.0f, 20.0f });

		// MODEL LOADING
		if (ImGui::Button("Load a new model...", ImVec2(-FLT_MIN, ImGui::GetTextLineHeightWithSpacing())))
		{
			mFileDialog.Open();
		}
		ImGui::Dummy({ 0.0f, 20.0f });

		if (ImGui::CollapsingHeader("Model", ImGuiTreeNodeFlags_DefaultOpen))
		{
			ImGui::Dummy({ 0.0f, 10.0f });
			ImGui::Columns(2, "", false);
			ImGui::Text("Current model :");ImGui::NextColumn();ImGui::TextColored(ImVec4(0.75f, 0.0f, 0.0f, 1.0f), mCurrentFile.c_str());
			ImGui::Columns(1);
			ImGui::Dummy({ 0.0f, 5.0f });
			ImGui::Checkbox("Rotate", &scene->rotate);
		}

		// MATERIAL
		if (model)
		{
			ImGui::Dummy({ 0.0f, 10.0f });
			if (ImGui::CollapsingHeader("Material", ImGuiTreeNodeFlags_DefaultOpen))
			{
				ImGui::Columns(1, "material");

				// Albedo
				ImGui::Dummy({ 0.0f, 10.0f });
				if (ImGui::TreeNode("Albedo")) {

					ImGui::Dummy({ 0.0f, 5.0f });
					if (ImGui::Button("Load albedo texture..."))
					{
						mTextureFileDialog.Open();
						textureBeingLoadedType = nmaterial::ETextureType::DIFFUSE;
					}
					ImGui::Dummy({ 0.0f, 5.0f });
					if (!model->mMaterial.getDiffuseTexture())
					{
						ImGui::Columns(2, "", false);
						ImGui::Text("Current texture :");ImGui::NextColumn();ImGui::Text(mCurrentDiffuseTexture.c_str());
						ImGui::Columns(1);

						ImGui::Dummy({ 0.0f, 5.0f });
						if (ImGui::TreeNode("ColorPicker"))
						{
							ImGui::ColorPicker3("Color", (float*)&model->mMaterial.mColor, ImGuiColorEditFlags_PickerHueWheel | ImGuiColorEditFlags_DisplayRGB);
							ImGui::TreePop();
						}
					}
					else
					{
						ImGui::Columns(2, "", false);
						ImGui::Text("Current texture :");ImGui::NextColumn();ImGui::Text(mCurrentDiffuseTexture.c_str());
						ImGui::Columns(1);
						ImGui::Dummy({ 0.0f, 10.0f });
						ImGui::Columns(2, "", false);
						if (ImGui::Checkbox("Flip", &model->mMaterial.getDiffuseTexture()->flip))
						{
							model->mMaterial.getDiffuseTexture()->reload();
						}
						ImGui::NextColumn();
						ImGui::Checkbox("Active", &model->mMaterial.getDiffuseTexture()->activated);
						ImGui::Columns(1);
					}
					ImGui::TreePop();
				}

				ImGui::Separator();

				// Normal
				ImGui::Dummy({ 0.0f, 10.0f });
				if (ImGui::TreeNode("Normal")) {

					ImGui::Dummy({ 0.0f, 5.0f });
					if (ImGui::Button("Load normal texture..."))
					{
						mTextureFileDialog.Open();
						textureBeingLoadedType = nmaterial::ETextureType::NORMAL;
					}
					ImGui::Dummy({ 0.0f, 5.0f });
					if (model->mMaterial.getNormalTexture())
					{
						ImGui::Columns(2, "", false);
						ImGui::Text("Current texture :");ImGui::NextColumn();ImGui::Text(mCurrentNormalTexture.c_str());
						ImGui::Dummy({ 0.0f, 10.0f });
						ImGui::Columns(1);
						ImGui::Columns(2, "", false);
						if (ImGui::Checkbox("Flip", &model->mMaterial.getNormalTexture()->flip))
						{
							model->mMaterial.getNormalTexture()->reload();
						}
						ImGui::NextColumn();
						ImGui::Checkbox("Active", &model->mMaterial.getNormalTexture()->activated);
						ImGui::Columns(1);
					}
					else
					{
						ImGui::Columns(2, "", false);
						ImGui::Text("Current texture :");ImGui::NextColumn();ImGui::Text(mCurrentNormalTexture.c_str());
						ImGui::Columns(1);
					}
					ImGui::TreePop();
				}

				ImGui::Separator();

				// Roughness
				ImGui::Dummy({ 0.0f, 10.0f });
				if (ImGui::TreeNode("Roughness")) {

					ImGui::Dummy({ 0.0f, 5.0f });
					if (ImGui::Button("Load Roughness texture..."))
					{
						mTextureFileDialog.Open();
						textureBeingLoadedType = nmaterial::ETextureType::ROUGHNESS;
					}
					ImGui::Dummy({ 0.0f, 5.0f });
					if (!model->mMaterial.getRoughnessTexture())
					{
						ImGui::Columns(2, "", false);
						ImGui::Text("Current texture :");ImGui::NextColumn();ImGui::Text(mCurrentRoughnessTexture.c_str());
						ImGui::Columns(1);
						ImGui::Dummy({ 0.0f, 5.0f });
						if (ImGui::TreeNode("Set value"))
						{
							ImGui::SliderFloat("Rougness", &model->mMaterial.mRoughness, 0.0f, 1.0f);
							ImGui::TreePop();
						}
					}
					else
					{
						ImGui::Columns(2, "", false);
						ImGui::Text("Current texture :");ImGui::NextColumn();ImGui::Text(mCurrentRoughnessTexture.c_str());
						ImGui::Columns(1);
						ImGui::Dummy({ 0.0f, 10.0f });
						ImGui::Columns(2, "", false);
						if (ImGui::Checkbox("Flip", &model->mMaterial.getRoughnessTexture()->flip))
						{
							model->mMaterial.getRoughnessTexture()->reload();
						}
						ImGui::NextColumn();
						ImGui::Checkbox("Active", &model->mMaterial.getRoughnessTexture()->activated);
						ImGui::Columns(1);
					}
					ImGui::TreePop();
				}

				ImGui::Separator();

				// Metallic
				ImGui::Dummy({ 0.0f, 10.0f });
				if (ImGui::TreeNode("Metallic")) {

					ImGui::Dummy({ 0.0f, 5.0f });
					if (ImGui::Button("Load Metallic texture..."))
					{
						mTextureFileDialog.Open();
						textureBeingLoadedType = nmaterial::ETextureType::METALLIC;
					}
					ImGui::Dummy({ 0.0f, 5.0f });
					if (!model->mMaterial.getMetallicTexture())
					{
						ImGui::Columns(2, "", false);
						ImGui::Text("Current texture :");ImGui::NextColumn();ImGui::Text(mCurrentMetallicTexture.c_str());
						ImGui::Columns(1);
						ImGui::Dummy({ 0.0f, 5.0f });
						if (ImGui::TreeNode("Set value"))
						{
							ImGui::SliderFloat("Metallic", &model->mMaterial.mMetallic, 0.0f, 1.0f);
							ImGui::TreePop();
						}
					}
					else
					{
						ImGui::Columns(2, "", false);
						ImGui::Text("Current texture :");ImGui::NextColumn();ImGui::Text(mCurrentMetallicTexture.c_str());
						ImGui::Columns(1);
						ImGui::Dummy({ 0.0f, 10.0f });
						ImGui::Columns(2, "", false);
						if (ImGui::Checkbox("Flip", &model->mMaterial.getMetallicTexture()->flip))
						{
							model->mMaterial.getMetallicTexture()->reload();
						}
						ImGui::NextColumn();
						ImGui::Checkbox("Active", &model->mMaterial.getMetallicTexture()->activated);
						ImGui::Columns(1);
					}
					ImGui::TreePop();
				}

				ImGui::Separator();

				// AO
				ImGui::Dummy({ 0.0f, 10.0f });
				if (ImGui::TreeNode("Ambient occlusion")) {

					ImGui::Dummy({ 0.0f, 5.0f });
					if (ImGui::Button("Load AO texture..."))
					{
						mTextureFileDialog.Open();
						textureBeingLoadedType = nmaterial::ETextureType::AMBIENT_OCCLUSION;
					}
					ImGui::Dummy({ 0.0f, 5.0f });
					if (!model->mMaterial.getAmbienOcclusionTexture())
					{
						ImGui::Columns(2, "", false);
						ImGui::Text("Current texture :");ImGui::NextColumn();ImGui::Text(mCurrentAOTexture.c_str());
						ImGui::Columns(1);
						ImGui::Dummy({ 0.0f, 5.0f });
						if (ImGui::TreeNode("Set value"))
						{
							ImGui::SliderFloat("Ambient occlusion", &model->mMaterial.mAo, 0.0f, 1.0f);
							ImGui::TreePop();
						}
					}
					else
					{
						ImGui::Columns(2, "", false);
						ImGui::Text("Current texture :");ImGui::NextColumn();ImGui::Text(mCurrentAOTexture.c_str());
						ImGui::Columns(1);
						ImGui::Dummy({ 0.0f, 10.0f });
						ImGui::Columns(2, "", false);
						if (ImGui::Checkbox("Flip", &model->mMaterial.getAmbienOcclusionTexture()->flip))
						{
							model->mMaterial.getAmbienOcclusionTexture()->reload();
						}
						ImGui::NextColumn();
						ImGui::Checkbox("Active", &model->mMaterial.getAmbienOcclusionTexture()->activated);
						ImGui::Columns(1);
					}
					ImGui::TreePop();
				}

				ImGui::Separator();

				// Height
				ImGui::Dummy({ 0.0f, 10.0f });
				if (ImGui::TreeNode("Height")) {

					ImGui::Dummy({ 0.0f, 5.0f });
					if (ImGui::Button("Load Height texture..."))
					{
						mTextureFileDialog.Open();
						textureBeingLoadedType = nmaterial::ETextureType::HEIGHT;
					}
					ImGui::Dummy({ 0.0f, 5.0f });
					if (model->mMaterial.getHeightTexture())
					{
						ImGui::Columns(2, "", false);
						ImGui::Text("Current texture :");ImGui::NextColumn();ImGui::Text(mCurrentHeightTexture.c_str());
						ImGui::Columns(1);
						ImGui::Dummy({ 0.0f, 10.0f });
						ImGui::Columns(2, "", false);
						if (ImGui::Checkbox("Flip", &model->mMaterial.getHeightTexture()->flip))
						{
							model->mMaterial.getHeightTexture()->reload();
						}
						ImGui::NextColumn();
						ImGui::Checkbox("Active", &model->mMaterial.getHeightTexture()->activated);
						ImGui::Columns(1);
					}
					else
					{
						ImGui::Columns(2, "", false);
						ImGui::Text("Current texture :");ImGui::NextColumn();ImGui::Text(mCurrentHeightTexture.c_str());
						ImGui::Columns(1);
					}
					ImGui::TreePop();
				}

				ImGui::Columns(1);
				ImGui::Separator();
			}
		}

		// LIGHT
		ImGui::Dummy({ 0.0f, 20.0f });
		if (ImGui::CollapsingHeader("Light"))
		{
			ImGui::Dummy({ 0.0f, 10.0f });
			ImGui::Text("Position");
			ImGui::SliderFloat3("Position", (float*)&scene->mLightPosition, -10.0f, 10.0f);

			ImGui::Dummy({ 0.0f, 10.0f });
			ImGui::Text("Color");
			ImGui::ColorPicker3("Color", (float*)&scene->mLightColor, ImGuiColorEditFlags_PickerHueWheel | ImGuiColorEditFlags_DisplayRGB);
		}

		// HEIGHT
		ImGui::Dummy({ 0.0f, 20.0f });
		if (ImGui::CollapsingHeader("Height demo"))
		{
			ImGui::Dummy({ 0.0f, 10.0f });
			if (ImGui::Checkbox("Show height demo 1: Brick wall", &scene->mDrawHeigthDemo))
			{
				if (scene->mDrawHeigthDemo)
				{
					model.reset();
					model = std::make_shared<nelements::Model>();
					model->mMaterial = nmaterial::Material();
					// Load the textures
					scene->load_texture("resources/textures/pbr/wall/albedo.png", nmaterial::ETextureType::DIFFUSE);
					scene->load_texture("resources/textures/pbr/wall/normal.png", nmaterial::ETextureType::NORMAL);
					scene->load_texture("resources/textures/pbr/wall/roughness.png", nmaterial::ETextureType::ROUGHNESS);
					scene->load_texture("resources/textures/pbr/wall/metallic.png", nmaterial::ETextureType::METALLIC);
					scene->load_texture("resources/textures/pbr/wall/ao.png", nmaterial::ETextureType::AMBIENT_OCCLUSION);
					scene->load_texture("resources/textures/pbr/wall/height.png", nmaterial::ETextureType::HEIGHT);
				}
			}

			ImGui::Dummy({ 0.0f, 10.0f });
			if (ImGui::Checkbox("Show height demo 2: Wool", &scene->mDrawHeigthDemo))
			{
				if (scene->mDrawHeigthDemo)
				{
					model.reset();
					model = std::make_shared<nelements::Model>();
					model->mMaterial = nmaterial::Material();
					// Load the textures
					scene->load_texture("resources/textures/pbr/wool/TexturesCom_Fabric_Wool5_512_albedo.png", nmaterial::ETextureType::DIFFUSE);
					scene->load_texture("resources/textures/pbr/wool/TexturesCom_Fabric_Wool5_512_normal.png", nmaterial::ETextureType::NORMAL);
					scene->load_texture("resources/textures/pbr/wool/TexturesCom_Fabric_Wool5_512_roughness.png", nmaterial::ETextureType::ROUGHNESS);
					scene->load_texture("resources/textures/pbr/wool/TexturesCom_Fabric_Wool5_512_ao.png", nmaterial::ETextureType::AMBIENT_OCCLUSION);
					scene->load_texture("resources/textures/pbr/wool/TexturesCom_Fabric_Wool5_512_height.png", nmaterial::ETextureType::HEIGHT);
				}
			}

			ImGui::Dummy({ 0.0f, 10.0f });
			if (ImGui::Checkbox("Show height demo 3: Rock", &scene->mDrawHeigthDemo))
			{
				if (scene->mDrawHeigthDemo)
				{
					model.reset();
					model = std::make_shared<nelements::Model>();
					model->mMaterial = nmaterial::Material();
					// Load the textures
					scene->load_texture("resources/textures/pbr/rock/TexturesCom_Rock_CliffBasalt_1K_albedo.png", nmaterial::ETextureType::DIFFUSE);
					scene->load_texture("resources/textures/pbr/rock/TexturesCom_Rock_CliffBasalt_1K_normal.png", nmaterial::ETextureType::NORMAL);
					scene->load_texture("resources/textures/pbr/rock/TexturesCom_Rock_CliffBasalt_1K_roughness.png", nmaterial::ETextureType::ROUGHNESS);
					scene->load_texture("resources/textures/pbr/rock/TexturesCom_Rock_CliffBasalt_1K_ao.png", nmaterial::ETextureType::AMBIENT_OCCLUSION);
					scene->load_texture("resources/textures/pbr/rock/TexturesCom_Rock_CliffBasalt_1K_height.png", nmaterial::ETextureType::HEIGHT);
				}
			}

			ImGui::Dummy({ 0.0f, 10.0f });
			if (ImGui::Checkbox("Show height demo 4: Muddy road", &scene->mDrawHeigthDemo))
			{
				if (scene->mDrawHeigthDemo)
				{
					model.reset();
					model = std::make_shared<nelements::Model>();
					model->mMaterial = nmaterial::Material();
					// Load the textures
					scene->load_texture("resources/textures/pbr/mud/TexturesCom_Road_Mud_512_albedo.png", nmaterial::ETextureType::DIFFUSE);
					scene->load_texture("resources/textures/pbr/mud/TexturesCom_Road_Mud_512_normal.png", nmaterial::ETextureType::NORMAL);
					scene->load_texture("resources/textures/pbr/mud/TexturesCom_Road_Mud_512_roughness.png", nmaterial::ETextureType::ROUGHNESS);
					scene->load_texture("resources/textures/pbr/mud/TexturesCom_Road_Mud_512_ao.png", nmaterial::ETextureType::AMBIENT_OCCLUSION);
					scene->load_texture("resources/textures/pbr/mud/TexturesCom_Road_Mud_512_height.png", nmaterial::ETextureType::HEIGHT);
				}
			}
		}

		ImGui::End();

		// FILE DIALOGS
		mFileDialog.Display();
		if (mFileDialog.HasSelected())
		{
			auto file_path = mFileDialog.GetSelected().string();
			mCurrentFile = file_path.substr(file_path.find_last_of("/\\") + 1);

			mCurrentDiffuseTexture = "<...>";
			mCurrentNormalTexture = "<...>";
			mCurrentRoughnessTexture = "<...>";
			mCurrentMetallicTexture = "<...>";
			mCurrentHeightTexture = "<...>";
			mCurrentAOTexture = "<...>";
			mMeshLoadCallback(file_path);

			mFileDialog.ClearSelected();
		}

		mTextureFileDialog.Display();
		if (mTextureFileDialog.HasSelected())
		{
			auto file_path = mTextureFileDialog.GetSelected().string();
			switch (textureBeingLoadedType)
			{
			case nmaterial::ETextureType::DIFFUSE:
				mCurrentDiffuseTexture = file_path.substr(file_path.find_last_of("/\\") + 1);
				break;
			case nmaterial::ETextureType::NORMAL:
				mCurrentNormalTexture = file_path.substr(file_path.find_last_of("/\\") + 1);
				break;
			case nmaterial::ETextureType::ROUGHNESS:
				mCurrentRoughnessTexture = file_path.substr(file_path.find_last_of("/\\") + 1);
				break;
			case nmaterial::ETextureType::AMBIENT_OCCLUSION:
				mCurrentAOTexture = file_path.substr(file_path.find_last_of("/\\") + 1);
				break;
			case nmaterial::ETextureType::HEIGHT:
				mCurrentHeightTexture = file_path.substr(file_path.find_last_of("/\\") + 1);
				break;
			case nmaterial::ETextureType::METALLIC:
				mCurrentMetallicTexture = file_path.substr(file_path.find_last_of("/\\") + 1);
				break;
			default:
				break;
			}
			mTextureLoadCallback(file_path, textureBeingLoadedType);

			mTextureFileDialog.ClearSelected();
		}
	}
}