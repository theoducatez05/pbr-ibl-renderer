/*********************************************************************
 * @file   ibl_panel.cpp
 * @brief  The IBL panel class, containing all the UI ImGUI properties
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#include "pch.h"

#include "ibl_panel.h"

namespace nui
{
	IBLPanel::IBLPanel()
	{
		// Load the textures for thumbnails
		thumbnail1 = std::make_shared<nmaterial::Texture>("resources/textures/hdr/Alexs_Apt_Thumb.jpg", nmaterial::ETextureType::DIFFUSE, false);
		thumbnail2 = std::make_shared<nmaterial::Texture>("resources/textures/hdr/Factory_Catwalk_Thumb.jpg", nmaterial::ETextureType::DIFFUSE, false);
		thumbnail3 = std::make_shared<nmaterial::Texture>("resources/textures/hdr/Tropical_Beach_thumb.jpg", nmaterial::ETextureType::DIFFUSE, false);
	}

	void IBLPanel::render(nscene::Scene* scene)
	{
		ImGui::Begin("IBL Panel");

		ImGui::Dummy({ 0.0f, 10.0f });

		ImVec2 size = ImVec2(128.0f, 128.0f);                   // Size of the image we want to make visible
		ImVec2 uv0 = ImVec2(0.0f, 0.0f);                        // UV coordinates for lower-left
		ImVec2 uv1 = ImVec2(1.0f, 1.0f);						// UV coordinates for (32,32) in our texture
		ImVec4 bg_col = ImVec4(0.0f, 0.0f, 0.0f, 1.0f);         // Black background
		ImVec4 tint_col = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);       // No tint

		ImGui::Columns(4, "IBL", false);

		if (ImGui::ImageButton((ImTextureID)thumbnail1->getId(), size, uv0, uv1, -1, bg_col, tint_col))
		{
			scene->currentEnvCubemap = scene->envCubemap1;
			scene->currentIrradianceMap = scene->irradianceMap1;
			scene->currentPrefilterMap = scene->prefilterMap1;
		}
		ImGui::Text("House");
		ImGui::NextColumn();
		if (ImGui::ImageButton((ImTextureID)thumbnail2->getId(), size, uv0, uv1, -1, bg_col, tint_col))
		{
			scene->currentEnvCubemap = scene->envCubemap2;
			scene->currentIrradianceMap = scene->irradianceMap2;
			scene->currentPrefilterMap = scene->prefilterMap2;
		}
		ImGui::Text("Factory");
		ImGui::NextColumn();
		if (ImGui::ImageButton((ImTextureID)thumbnail3->getId(), size, uv0, uv1, -1, bg_col, tint_col))
		{
			scene->currentEnvCubemap = scene->envCubemap3;
			scene->currentIrradianceMap = scene->irradianceMap3;
			scene->currentPrefilterMap = scene->prefilterMap3;

		}
		ImGui::Text("Beach");
		ImGui::NextColumn();

		ImGui::Text("Change the skybox");
		ImGui::Dummy({ 0.0f, 5.0f });
		if (ImGui::RadioButton("Normal", scene->skyboxMode == 1)) { scene->skyboxMode = 1; }
		ImGui::Dummy({ 0.0f, 5.0f });
		if (ImGui::RadioButton("Irradiance", scene->skyboxMode == 2)) { scene->skyboxMode = 2; }
		ImGui::Dummy({ 0.0f, 5.0f });
		if (ImGui::RadioButton("Prefilter", scene->skyboxMode == 3)) { scene->skyboxMode = 3; }

		ImGui::Columns(1);

		ImGui::End();
	}
}