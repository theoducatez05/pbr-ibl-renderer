/*********************************************************************
 * @file   ibl_panel.h
 * @brief  The IBL panel class, containing all the UI ImGUI properties
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include "scene/scene.h"

#include "imgui.h"
#include <ImFileBrowser.h>

namespace nui
{
	class IBLPanel
	{
	public:

		IBLPanel();

		void render(nscene::Scene* mScene);

	private:

		// Thumbnails textures
		std::shared_ptr<nmaterial::Texture> thumbnail1;
		std::shared_ptr<nmaterial::Texture> thumbnail2;
		std::shared_ptr<nmaterial::Texture> thumbnail3;
	};
}
