/*********************************************************************
 * @file   property_panel.h
 * @brief  The property panel class, containing all the UI ImGUI properties
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include "scene/scene.h"

#include "imgui.h"
#include <ImFileBrowser.h>

namespace nui
{
	class Property_Panel
	{
	public:

		Property_Panel();

		void render(nscene::Scene* mScene);

		void set_mesh_load_callback(const std::function<void(const std::string&)>& callback)
		{
			mMeshLoadCallback = callback;
		}

		void set_texture_load_callback(const std::function<void(const std::string&, nmaterial::ETextureType type)>& callback)
		{
			mTextureLoadCallback = callback;
		}

	private:
		// Create for 3D model loading
		ImGui::FileBrowser mFileDialog;
		std::function<void(const std::string&)> mMeshLoadCallback;
		std::string mCurrentFile = "< ... >";

		// For textures loading
		ImGui::FileBrowser mTextureFileDialog;
		nmaterial::ETextureType textureBeingLoadedType;
		std::function<void(const std::string&, nmaterial::ETextureType type)> mTextureLoadCallback;
		std::string mCurrentDiffuseTexture = "< ... >";
		std::string mCurrentNormalTexture = "< ... >";
		std::string mCurrentRoughnessTexture = "< ... >";
		std::string mCurrentMetallicTexture = "< ... >";
		std::string mCurrentAOTexture = "< ... >";
		std::string mCurrentHeightTexture = "< ... >";
	};
}
