/*********************************************************************
 * @file   ibl_loader.h
 * @brief  IBL HDR texture loading and pre-computing class creator
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include"pch.h"

#include "shader/shader.h"

class IBLLoader
{
public:
	IBLLoader(std::string hdrTexturePath);

	GLuint setupHDRCubemap(nshader::Shader* shader);

	GLuint setupIrradianceCubemap();

	GLuint setupPrefilterCubemap();

	GLuint setupLutBRDFTexture();

private:

	std::string hdrTexturePath;

	// Our frame buffer indices
	unsigned int captureFBO;
	unsigned int captureRBO;

	// Our IBL textures indices
	unsigned int hdrTexture;
	unsigned int envCubemap;
	unsigned int irradianceMap;
	unsigned int prefilterMap;
	unsigned int brdfLUTTexture;

	// Used for hdr cubemap creation
	glm::mat4 captureProjection;
	glm::mat4 captureViews[6];

	// The shaders for pre-computation
	std::unique_ptr<nshader::Shader> mEquirectangularToCubemapShader;
	std::unique_ptr<nshader::Shader> mIrradianceShader;
	std::unique_ptr<nshader::Shader> mPrefilterShader;
	std::unique_ptr<nshader::Shader> mBrdfShader;
};

