/*********************************************************************
 * @file   scene.cpp
 * @brief  The OpenGL viewport in the opengl context UI panel
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#include "pch.h"

#include "scene.h"

#include "stb_image/stb_image.h"
#include "vendor/imgui-docking/imgui.h"

#include "ibl/ibl_loader.h"
#include "util/render_objects_util.h"
#include "input/input.h"

namespace nscene
{
	void Scene::load_model(const std::string& filepath)
	{
        mModel.reset();
		mModel = std::make_shared<nelements::Model>(filepath);
	}

    void Scene::load_texture(const std::string& filepath, nmaterial::ETextureType type)
	{
        if (mModel)
        {
            mModel->mMaterial.setTexture(filepath, type, false);
        }
	}

    void Scene::set_projection(int width, int height)
    {
        mSize.x = width;
        mSize.y = height;

        mFrameBuffer->create_buffers((int32_t)mSize.x, (int32_t)mSize.y);

        mCamera->set_aspect((float)width / (float)height);
        projection = mCamera->get_projection();
    }

    void Scene::on_mouse_move(double x, double y, ninput::EInputButton button)
    {
        mCamera->on_mouse_move(x, y, button);
    }

    void Scene::on_mouse_wheel(double delta)
    {
        mCamera->on_mouse_wheel(delta);
    }

	Scene::Scene(int width, int height) : mModel(nullptr)
	{
        mFrameBuffer = std::make_unique<nrender::FrameBuffer>();
        mFrameBuffer->create_buffers(width, height);

		// Create the light
		mLightPosition = glm::vec3(-1.0f, 1.0f, 1.0f);
		mLightColor = glm::vec3(1.0f, 1.0f, 1.0f);

        // Create the camera
        mCamera = std::make_unique<nelements::Camera>(glm::vec3(0.0f, 0.0f, 3.0f), 45.0f, (float)width/ (float)height, 0.1f, 100.0f);

		// Create the shaders for rendering
		mPbrShader = std::make_unique<nshader::Shader>("resources/shaders/pbr.vert.glsl", "resources/shaders/pbr.frag.glsl");
		mBackgroundShader = std::make_unique<nshader::Shader>("resources/shaders/background.vert.glsl", "resources/shaders/background.frag.glsl");
		mLightShader = std::make_unique<nshader::Shader>("resources/shaders/light.vert.glsl", "resources/shaders/light.frag.glsl");

        // Start the HDR process for 1st map
        IBLLoader iblLoader("resources/textures/hdr/Alexs_Apt_2k.hdr");
        // Get cubemap
        envCubemap1 = iblLoader.setupHDRCubemap(mBackgroundShader.get());
        currentEnvCubemap = envCubemap1;
        // Get prefiltered
        irradianceMap1 = iblLoader.setupIrradianceCubemap();
        currentIrradianceMap = irradianceMap1;
        // Get prefilter
        prefilterMap1 = iblLoader.setupPrefilterCubemap();
        currentPrefilterMap = prefilterMap1;
        // Get brdf lut
        brdfLUTTexture = iblLoader.setupLutBRDFTexture();

        // 2nd map
        iblLoader = IBLLoader("resources/textures/hdr/Factory_Catwalk_2k.hdr");
        // Get cubemap
        envCubemap2 = iblLoader.setupHDRCubemap(mBackgroundShader.get());
        // Get prefiltered
        irradianceMap2 = iblLoader.setupIrradianceCubemap();
        // Get prefilter
        prefilterMap2 = iblLoader.setupPrefilterCubemap();

        // 2nd map
        iblLoader = IBLLoader("resources/textures/hdr/Tropical_Beach_3k.hdr");
        // Get cubemap
        envCubemap3 = iblLoader.setupHDRCubemap(mBackgroundShader.get());
        // Get prefiltered
        irradianceMap3 = iblLoader.setupIrradianceCubemap();
        // Get prefilter
        prefilterMap3 = iblLoader.setupPrefilterCubemap();

        // then before rendering, configure the viewport to the original framebuffer's screen dimensions
        glViewport(0, 0, width, height);

        // configure view/projection matrices
        projection = mCamera->get_projection();
	}

	void Scene::render()
	{
        glm::mat4 view = mCamera->get_view_matrix();
        glm::mat4 model;

        mFrameBuffer->bind();

        // Draw Model
        if (mModel)
        {
            mPbrShader->use();
            mPbrShader->setMat4("projection", projection);
            mPbrShader->setMat4("view", view);

            model = glm::mat4(1.0f);
            if(rotate)
                model = glm::rotate(model, glm::radians((float)glfwGetTime() * -10.0f), glm::normalize(glm::vec3(0.0, 1.0, 0.0))); // rotate the quad to show parallax mapping from multiple directions
            mPbrShader->setMat4("model", model);
            mPbrShader->setVec3("viewPos", mCamera->get_position());
            mPbrShader->setVec3("lightPos", mLightPosition);
            mPbrShader->setFloat("heightScale", 0.1f);
            mPbrShader->setVec3("lightColor", mLightColor);

            // Set material
            mPbrShader->setVec3("material.diffuse.color", mModel->mMaterial.mColor);
            mPbrShader->setVec3("material.roughness.color", glm::vec3(mModel->mMaterial.mRoughness));
            mPbrShader->setVec3("material.ao.color", glm::vec3(mModel->mMaterial.mAo));
            mPbrShader->setVec3("material.metallic.color", glm::vec3(mModel->mMaterial.mMetallic));
            mPbrShader->setVec3("material.height.color", { 0.0f, 0.0f, 0.0f });

            // bind pre-computed IBL data
            glActiveTexture(GL_TEXTURE6);
            mPbrShader->setInt("irradianceMap", 6);
            glBindTexture(GL_TEXTURE_CUBE_MAP, currentIrradianceMap);
            glActiveTexture(GL_TEXTURE7);
            mPbrShader->setInt("prefilterMap", 7);
            glBindTexture(GL_TEXTURE_CUBE_MAP, currentPrefilterMap);
            glActiveTexture(GL_TEXTURE8);
            mPbrShader->setInt("brdfLUT", 8);
            glBindTexture(GL_TEXTURE_2D, brdfLUTTexture);

            if (mDrawHeigthDemo)
            {
                mModel->activateTextures(*mPbrShader.get());
                nutil::renderQuad();
            }
            else
            {
                mModel->draw(*mPbrShader.get());
            }
        }

        // Render the light
        mLightShader->use();
        mLightShader->setMat4("projection", projection);
        mLightShader->setMat4("view", view);
        model = glm::mat4(1.0f);
        model = glm::translate(model, mLightPosition);
        model = glm::scale(model, glm::vec3(0.1f));
        mLightShader->setMat4("model", model);
        mLightShader->setVec3("color", mLightColor);
        nutil::renderCube();

        // Render skybox (render as last to prevent overdraw)
        mBackgroundShader->use();
        mBackgroundShader->setMat4("projection", projection);
        mBackgroundShader->setMat4("view", view);
        glActiveTexture(GL_TEXTURE0);
        if(skyboxMode == 1){
            glBindTexture(GL_TEXTURE_CUBE_MAP, currentEnvCubemap);
        }
        if (skyboxMode == 2) {
            glBindTexture(GL_TEXTURE_CUBE_MAP, currentIrradianceMap); // display irradiance map
        }
        if (skyboxMode == 3) {
            glBindTexture(GL_TEXTURE_CUBE_MAP, currentPrefilterMap); // display prefilter map
        }
        nutil::renderCube();

        mFrameBuffer->unbind();

        // Draw to gui texture image panel
        ImGui::Begin("Scene");

        ImVec2 viewportPanelSize = ImGui::GetContentRegionAvail();
        mSize = { viewportPanelSize.x, viewportPanelSize.y };

        mCamera->set_aspect(viewportPanelSize.x / viewportPanelSize.y);
        projection = mCamera->get_projection();

        // Add rendered texture to ImGUI scene window
        uint64_t textureID = mFrameBuffer->get_texture();
        ImGui::Image(reinterpret_cast<void*>(textureID), ImVec2{ mSize.x, mSize.y }, ImVec2{ 0, 1 }, ImVec2{ 1, 0 });

        ImGui::End();
    }
}