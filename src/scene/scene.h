/*********************************************************************
 * @file   scene.h
 * @brief  The OpenGL viewport in the opengl context UI panel
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include "elements/camera.h"
#include "elements/model.h"
#include "shader/shader.h"
#include "input/input.h"
#include "render/frame_buffer.h"

namespace nscene
{
	class Scene
	{
	public:
		Scene(int width, int height);

		void render();

		void load_model(const std::string& filepath);
		void load_texture(const std::string& filepath, nmaterial::ETextureType type);

		void set_model(std::shared_ptr<nelements::Model> model)
		{
			mModel = model;
		}

		void on_mouse_move(double x, double y, ninput::EInputButton button);

		void on_mouse_wheel(double delta);

		void reset_view()
		{
			mCamera->reset();
		}

		void set_projection(int width, int height);

		std::shared_ptr<nelements::Model> get_model() { return mModel; }
		glm::vec3 get_light_position() { return mLightPosition; }
		glm::vec3 get_light_color() { return mLightColor; }

		// Our one light position
		glm::vec3 mLightPosition;
		glm::vec3 mLightColor;

		// Our IBL textures indices
		unsigned int currentEnvCubemap;
		unsigned int currentIrradianceMap;
		unsigned int currentPrefilterMap;

		// Our different ones
		// 1
		unsigned int envCubemap1;
		unsigned int irradianceMap1;
		unsigned int prefilterMap1;

		//2
		unsigned int envCubemap2;
		unsigned int irradianceMap2;
		unsigned int prefilterMap2;

		//3
		unsigned int envCubemap3;
		unsigned int irradianceMap3;
		unsigned int prefilterMap3;

		// For skyboxes mode with the UI
		int skyboxMode = 1;

		// rotate the object
		bool rotate = false;

		// Height demo activated?
		bool mDrawHeigthDemo = false;

	private:

		// Our only model
		std::shared_ptr<nelements::Model> mModel;

		// Our only camera
		std::shared_ptr<nelements::Camera> mCamera;

		// The projection matrix
		glm::mat4 projection;

		// Our shaders
		std::unique_ptr<nshader::Shader> mPbrShader;
		std::unique_ptr<nshader::Shader> mEquirectangularToCubemapShader;
		std::unique_ptr<nshader::Shader> mIrradianceShader;
		std::unique_ptr<nshader::Shader> mPrefilterShader;
		std::unique_ptr<nshader::Shader> mBrdfShader;
		std::unique_ptr<nshader::Shader> mBackgroundShader;
		std::unique_ptr<nshader::Shader> mLightShader;

		// Our frame buffer indices
		unsigned int captureFBO;
		unsigned int captureRBO;

		// The LUT texture for BRDF
		unsigned int brdfLUTTexture;

		std::unique_ptr<nrender::FrameBuffer> mFrameBuffer;
		glm::vec2 mSize;
	};
}
