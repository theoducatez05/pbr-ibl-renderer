/*********************************************************************
 * @file   render_objects_utils.h
 * @brief  Hard coded 3D objects for use in program (cube, plane and sphere)
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/


#pragma once

#include "pch.h"

namespace nutil
{
    // renders (and builds at first invocation) a sphere
    // -------------------------------------------------
    void renderSphere();

    // renderCube() renders a 1x1 3D cube in NDC.
    // -------------------------------------------------
    void renderCube();

    // renders a 1x1 quad in NDC with manually calculated tangent vectors
    // ------------------------------------------------------------------
    void renderQuad();
}