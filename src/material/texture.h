/*********************************************************************
 * @file   texture.h
 * @brief  Abstraction for a Texture in OpenGL
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include "pch.h"

namespace nmaterial {

	enum class ETextureType {
		DIFFUSE,
		NORMAL,
		ROUGHNESS,
		AMBIENT_OCCLUSION,
		HEIGHT,
		METALLIC
	};

	class Texture
	{
	public:
		Texture(std::string, ETextureType type, bool flip);
		~Texture();

		void load(bool flip);
		void reload();

		inline GLuint getId() { return mid; }

		bool flip;
		bool activated = true;

	private:
		GLuint mid;
		ETextureType mtype;
		std::string mpath;
	};
}
