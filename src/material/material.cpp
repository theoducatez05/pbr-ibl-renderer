/*********************************************************************
 * @file   material.cpp
 * @brief  Abstraction for a material in OpenGL
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#include "material.h"

nmaterial::Material::Material() :
	mdiffuseTexture(nullptr),
	mnormalMapTexture(nullptr),
	mroughnessTexture(nullptr),
	mmetallicTexture(nullptr),
	mambientOcclusionTexture(nullptr),
	mheightTexture(nullptr)
{
}

void nmaterial::Material::setTexture(std::string texturePath, ETextureType textureType, bool flip)
{
	switch (textureType)
	{
	case ETextureType::DIFFUSE:
		mdiffuseTexture = std::make_shared<Texture>(texturePath, textureType, flip);
		break;
	case ETextureType::NORMAL:
		mnormalMapTexture = std::make_shared<Texture>(texturePath, textureType, flip);
		break;
	case ETextureType::ROUGHNESS:
		mroughnessTexture = std::make_shared<Texture>(texturePath, textureType, flip);
		break;
	case ETextureType::AMBIENT_OCCLUSION:
		mambientOcclusionTexture = std::make_shared<Texture>(texturePath, textureType, flip);
		break;
	case ETextureType::HEIGHT:
		mheightTexture = std::make_shared<Texture>(texturePath, textureType, flip);
		break;
	case ETextureType::METALLIC:
		mmetallicTexture = std::make_shared<Texture>(texturePath, textureType, flip);
		break;
	default:
		break;
	}
}
