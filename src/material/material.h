/*********************************************************************
 * @file   material.h
 * @brief  Abstraction for a material in OpenGL
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include "pch.h"

#include "texture.h"

namespace nmaterial
{
	class Material
	{
	public:
		glm::vec3 mColor = { 0.6f, 0.6f, 0.6f };
		float mRoughness = 0.2f;
		float mMetallic = 0.1f;
		float mAo = 1.0f;

		Material();
		~Material() {};

		void setTexture(std::string texturePath, ETextureType textureType, bool flip);

		inline std::shared_ptr<Texture>& getDiffuseTexture() { return mdiffuseTexture; }
		inline std::shared_ptr<Texture>& getNormalTexture() { return mnormalMapTexture; }
		inline std::shared_ptr<Texture>& getRoughnessTexture() { return mroughnessTexture; }
		inline std::shared_ptr<Texture>& getAmbienOcclusionTexture() { return mambientOcclusionTexture; }
		inline std::shared_ptr<Texture>& getHeightTexture() { return mheightTexture; }
		inline std::shared_ptr<Texture>& getMetallicTexture() { return mmetallicTexture; }


	private:
		std::shared_ptr<Texture> mdiffuseTexture;
		std::shared_ptr<Texture> mnormalMapTexture;
		std::shared_ptr<Texture> mroughnessTexture;
		std::shared_ptr<Texture> mambientOcclusionTexture;
		std::shared_ptr<Texture> mheightTexture;
		std::shared_ptr<Texture> mmetallicTexture;
	};
}

