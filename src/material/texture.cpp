/*********************************************************************
 * @file   texture.cpp
 * @brief  Abstraction for a Texture in OpenGL
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#include "texture.h"

#include "stb_image/stb_image.h"

nmaterial::Texture::Texture(std::string path, ETextureType type, bool flip) : mpath(path), mtype(type), flip(flip)
{
	load(flip);
}

nmaterial::Texture::~Texture()
{
	glDeleteTextures(1, &mid);
}

void nmaterial::Texture::reload()
{
    glDeleteTextures(1, &mid);
    flip = flip;
    load(flip);
}

void nmaterial::Texture::load(bool flip)
{
	stbi_set_flip_vertically_on_load(flip);

    glGenTextures(1, &mid);

    int width, height, nrComponents;
    unsigned char* data = stbi_load(mpath.c_str(), &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format = 1;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, mid);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "Texture failed to load at path: " << mpath << std::endl;
        stbi_image_free(data);
    }
    stbi_set_flip_vertically_on_load(flip);
}
