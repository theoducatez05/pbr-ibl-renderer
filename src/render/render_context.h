/*********************************************************************
 * @file   render_context.h
 * @brief  The rendering abstract class (pre_render, post_render, init and render)
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/


#pragma once

#include "window/window.h"

namespace nrender
{
	class RenderContext
	{
	public:

		RenderContext() : mWindow(nullptr) {}

		virtual bool init(nwindow::IWindow* window)
		{
			mWindow = window;
			return true;
		}

		virtual void pre_render() = 0;

		virtual void post_render() = 0;

		virtual void end() = 0;

	protected:
		nwindow::IWindow* mWindow;
	};
}
