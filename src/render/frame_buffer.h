/*********************************************************************
 * @file   frame_buffer.h
 * @brief  Abstraction for a frame buffer
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include "pch.h"
namespace nrender
{
	class FrameBuffer
	{
	public:
		FrameBuffer() : mFBO{ 0 }, mDepthId{ 0 }
		{}

		virtual void create_buffers(int width, int height);

		virtual void delete_buffers();

		virtual void bind();

		virtual void unbind();

		virtual GLuint get_texture();

	protected:
		GLuint mFBO = 0;
		GLuint mTexId = 0;
		GLuint mDepthId = 0;
		GLuint mWidth = 0;
		GLuint mHeight = 0;
	};
}