/*********************************************************************
 * @file   ui_context.h
 * @brief  The ImGUI context of the application (ImGUI initialisation)
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include "render_context.h"

namespace nrender
{
	class UIContext : public RenderContext
	{
	public:

		bool init(nwindow::IWindow* window) override;

		void pre_render() override;

		void post_render() override;

		void end() override;
	};
}
