/*********************************************************************
 * @file   opengl_context.h
 * @brief  The OpenGL rendering context (initialisation and rendering)
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include "render_context.h"

namespace nrender
{
	class OpenGL_Context : public RenderContext
	{
	public:

		bool init(nwindow::IWindow* window) override;

		void pre_render() override;

		void post_render() override;

		void end() override;
	};
}
