/*****************************************************************//**
 * @file   application.h
 * @brief  The main application singleton instance
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include "window/gl_window.h"

/**
 * The main application singleton instance.
 */
class Application
{
public:
	Application(const std::string& app_name);

	static Application& Instance() { return *sInstance; }

	void loop();

private:
	static Application* sInstance;

	std::unique_ptr<nwindow::GLWindow> mWindow;
};
