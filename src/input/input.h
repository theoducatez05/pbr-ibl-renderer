/*********************************************************************
 * @file   input.h
 * @brief  The input interface for buttons form the mouse
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include "pch.h"

namespace ninput
{
    enum class EInputButton
    {
        Left = 0,
        Right = 1,
        Middle = 2,
        None = 9
    };

    class Input
    {
    public:
        static EInputButton GetPressedButton(GLFWwindow* window)
        {
            EInputButton result = EInputButton::None;

            if (glfwGetMouseButton(window, 0) == GLFW_PRESS)
                return EInputButton::Left;
            else if (glfwGetMouseButton(window, 1) == GLFW_PRESS)
                return EInputButton::Right;
            else if (glfwGetMouseButton(window, 2) == GLFW_PRESS)
                return EInputButton::Middle;

            return EInputButton::None;
        }
    };
}
