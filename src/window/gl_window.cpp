/*********************************************************************
 * @file   gl_window.cpp
 * @brief  The OpenGL window derived from window
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/


#include "pch.h"

#include "gl_window.h"

#include"input/input.h"

namespace nwindow
{
	bool GLWindow::init(int width, int height, const std::string& title)
	{
		Width = width;
		Height = height;
		Title = title;

		mRenderCtx->init(this);

		mUICtx->init(this);

		mScene = std::make_unique<nscene::Scene>(width, height);

		mPropertyPanel = std::make_unique<nui::Property_Panel>();

		mIBLPanel = std::make_unique<nui::IBLPanel>();

		mPropertyPanel->set_mesh_load_callback(
			[this](std::string filepath) { mScene->load_model(filepath); });

		mPropertyPanel->set_texture_load_callback(
			[this](std::string filepath, nmaterial::ETextureType type) { mScene->load_texture(filepath, type); });

		return mIsRunning;
	}

	GLWindow::~GLWindow()
	{
		mUICtx->end();

		mRenderCtx->end();
	}

	void GLWindow::on_resize(int width, int height)
	{
		Width = width;
		Height = height;

		render();
	}

	void GLWindow::on_scroll(double delta)
	{
		mScene->on_mouse_wheel(delta);
	}

	void GLWindow::on_key(int key, int scancode, int action, int mods)
	{
		if (action == GLFW_PRESS)
		{
			// Key press
		}
	}

	void GLWindow::on_close()
	{
		mIsRunning = false;
	}

	void GLWindow::render()
	{
		// Clear the view in the window
		mRenderCtx->pre_render();

		// Initialize UI components (ImGUI)
		mUICtx->pre_render();

		// Render scene + UI
		mScene->render();
		mPropertyPanel->render(mScene.get());
		mIBLPanel->render(mScene.get());

		// Post render the UI
		mUICtx->post_render();

		// Render end, swap buffers
		mRenderCtx->post_render();

		handle_input();
	}

	void GLWindow::handle_input()
	{
		if (glfwGetKey(mWindow, GLFW_KEY_W) == GLFW_PRESS)
		{
			mScene->on_mouse_wheel(-0.4f);
		}

		if (glfwGetKey(mWindow, GLFW_KEY_S) == GLFW_PRESS)
		{
			mScene->on_mouse_wheel(0.4f);
		}

		if (glfwGetKey(mWindow, GLFW_KEY_F) == GLFW_PRESS)
		{
			mScene->reset_view();
		}

		double x, y;
		glfwGetCursorPos(mWindow, &x, &y);

		mScene->on_mouse_move(x, y, ninput::Input::GetPressedButton(mWindow));
	}
}