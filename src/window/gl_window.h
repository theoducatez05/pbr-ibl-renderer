/*********************************************************************
 * @file   gl_window.h
 * @brief  The OpenGL window derived from window
 *
 * Final Assignment - Real Time Rendering - Trinity College 2021/2022
 *
 * @author Th�o Ducatez
 * @date   March 2022
 *********************************************************************/

#pragma once

#include "render/ui_context.h"
#include "render/opengl_context.h"

#include "ui/property_panel.h"
#include "ui/ibl_panel.h"

#include "scene/scene.h"

#include "window/window.h"

namespace nwindow
{
	class GLWindow : public IWindow
	{
	public:

		GLWindow() :
			mIsRunning(true), mWindow(nullptr)
		{
			mUICtx = std::make_unique<nrender::UIContext>();
			mRenderCtx = std::make_unique<nrender::OpenGL_Context>();
		}

		~GLWindow();

		bool init(int width, int height, const std::string& title);

		void render();

		void handle_input();

		void* get_native_window() override { return mWindow; }

		void set_native_window(void* window)
		{
			mWindow = (GLFWwindow*)window;
		}

		void on_scroll(double delta) override;

		void on_key(int key, int scancode, int action, int mods) override;

		void on_resize(int width, int height) override;

		void on_close() override;

		bool is_running() { return mIsRunning; }

	private:

		GLFWwindow* mWindow;

		// Render contexts
		std::unique_ptr<nrender::UIContext> mUICtx;
		std::unique_ptr<nrender::OpenGL_Context> mRenderCtx;

		// Scene (OpenGL viewport)
		std::unique_ptr<nscene::Scene> mScene;

		// UI
		std::unique_ptr<nui::Property_Panel> mPropertyPanel;
		std::unique_ptr<nui::IBLPanel> mIBLPanel;

		bool mIsRunning;
	};
}
