#version 330

layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec3 aNormals;
layout (location = 2) in vec2 aTextureCoordinates;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    // Vertex position
    gl_Position = projection * view * model * vec4(aPosition.x, aPosition.y, aPosition.z, 1.0);
}