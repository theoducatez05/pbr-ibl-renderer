#version 330 core
out vec4 FragColor;
in vec3 WorldPos;

uniform samplerCube environmentMap;

void main()
{
    vec3 envColor = textureLod(environmentMap, WorldPos, 0.0).rgb;

    // HDR tonemapping
    envColor = envColor / (envColor + vec3(1.0));

    // Gamma correcting the map
    envColor = pow(envColor, vec3(1.0/2.2));

    // Setting the colour
    FragColor = vec4(envColor, 1.0);
}

