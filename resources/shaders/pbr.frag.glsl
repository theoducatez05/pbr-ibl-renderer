#version 330 core
out vec4 FragColor;

in VS_OUT {
    vec3 FragPos;
    vec2 TexCoords;
    vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;

    vec3 normal;
    vec3 LightPos;
    vec3 ViewPos;
} fs_in;

struct MatElement{
    bool isTexture;
    vec3 color;
    sampler2D texture;
};

struct Material{
    MatElement diffuse;
    MatElement normal;
    MatElement height;
    MatElement metallic;
    MatElement roughness;
    MatElement ao;
};

// IBL
uniform samplerCube irradianceMap;
uniform samplerCube prefilterMap;
uniform sampler2D brdfLUT;

// Material
uniform Material material;

// Light
uniform vec3 lightColor = vec3(1.0f, 1.0f, 1.0f);

// Height
uniform float heightScale = 0.1;

const float PI = 3.14159265359;

/*
    Get the new normal taking into account the normal map
*/
vec3 getNormalFromMap(vec2 texCoord)
{
    vec3 tangentNormal = texture(material.normal.texture, texCoord).xyz * 2.0 - 1.0;

    vec3 Q1  = dFdx(fs_in.FragPos);
    vec3 Q2  = dFdy(fs_in.FragPos);
    vec2 st1 = dFdx(texCoord);
    vec2 st2 = dFdy(texCoord);

    vec3 N   = normalize(fs_in.normal);
    vec3 T  = normalize(Q1*st2.t - Q2*st1.t);
    vec3 B  = -normalize(cross(N, T));
    mat3 TBN = mat3(T, B, N);

    return normalize(TBN * tangentNormal);
}

/*
    Offsets the UV texture coordinates as given by the height map for parallax mapping
*/
vec2 ParallaxMapping(vec2 texCoords, vec3 viewDir)
{
    // number of depth layers
    const float minLayers = 32;
    const float maxLayers = 64;
    float numLayers = mix(maxLayers, minLayers, abs(dot(vec3(0.0, 0.0, 1.0), viewDir)));
    // calculate the size of each layer
    float layerDepth = 1.0 / numLayers;
    // depth of current layer
    float currentLayerDepth = 0.0;
    // the amount to shift the texture coordinates per layer (from vector P)
    vec2 P = viewDir.xy / viewDir.z * heightScale;
    vec2 deltaTexCoords = P / numLayers;

    // get initial values
    vec2  currentTexCoords     = texCoords;
    float currentDepthMapValue = texture(material.height.texture, currentTexCoords).r;

    while(currentLayerDepth < currentDepthMapValue)
    {
        // shift texture coordinates along direction of P
        currentTexCoords -= deltaTexCoords;
        // get depthmap value at current texture coordinates
        currentDepthMapValue = texture(material.height.texture, currentTexCoords).r;
        // get depth of next layer
        currentLayerDepth += layerDepth;
    }

    // get texture coordinates before collision (reverse operations)
    vec2 prevTexCoords = currentTexCoords + deltaTexCoords;

    // get depth after and before collision for linear interpolation
    float afterDepth  = currentDepthMapValue - currentLayerDepth;
    float beforeDepth = texture(material.height.texture, prevTexCoords).r - currentLayerDepth + layerDepth;

    // interpolation of texture coordinates
    float weight = afterDepth / (afterDepth - beforeDepth);
    vec2 finalTexCoords = prevTexCoords * weight + currentTexCoords * (1.0 - weight);

    return finalTexCoords;
}

/*
	Function that calculate the normal distribution function (D) in the equation

	Params:
		- N : vec3 : The normal
		- H : vec3 : The H parameter
        - roughness: float : The roughness

	Return:
		- The normal distribution
*/float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a = roughness*roughness;
    float a2 = a*a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float nom   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}

/*
	Function that calculate the geometry distribution with Schlick GGX method

	Params:
		- NdotV : float : NdotV
		- Roughness : float : The roughness

	Return:
		- GGX term
*/float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float nom   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}

/*
	Function that calculate the geometry function (G) in the equation

	Params:
		- N : vec3 : The normal
		- V : vec3 : The view direction
		- L : vec3 : The L term
		- float : roughness : The roughness

	Return:
		- The geometry distribution (G) in the equation
*/float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = GeometrySchlickGGX(NdotV, roughness);
    float ggx1 = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

/*
	Function that calculate the Fresnel ratio giving Theta the incident angle and F0

	Params:
		- cosTheta : float : The incident angle (with the normal and the view direction)
		- F0 : float : The F0 value

	Return:
		- The Fresnel ratio of reflectance (1 - this to have the refracted one)
*/
vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
}

/*
    Same as FresnelSchlick but with roughness taken into account
*/
vec3 fresnelSchlickRoughness(float cosTheta, vec3 F0, float roughness)
{
    return F0 + (max(vec3(1.0 - roughness), F0) - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
}

void main()
{
    // Offset texture coordinates with Parallax Mapping if needed
    vec3 viewDir = normalize(fs_in.ViewPos - fs_in.FragPos);

    vec3 lightDir = normalize(fs_in.LightPos - fs_in.FragPos);

    vec2 texCoords = fs_in.TexCoords;

    if(material.height.isTexture){
        texCoords = ParallaxMapping(fs_in.TexCoords,  normalize(fs_in.TangentViewPos - fs_in.TangentFragPos));
        if(texCoords.x > 1.0 || texCoords.y > 1.0 || texCoords.x < 0.0 || texCoords.y < 0.0)
            discard;
    }

    // Obtain normal from normal map
    vec3 normal;
    if(material.normal.isTexture){
        normal = getNormalFromMap(texCoords);
    }
    else
        normal = fs_in.normal;

    // Material properties
    vec3 albedo;
    if(material.diffuse.isTexture){
        albedo = pow(texture(material.diffuse.texture, texCoords).rgb, vec3(2.2));
    }
    else
        albedo = material.diffuse.color;

    float metallic;
    if(material.metallic.isTexture){
        metallic = texture(material.metallic.texture, texCoords).r;
    }
    else
        metallic = material.metallic.color.r;

    float roughness;
    if(material.roughness.isTexture){
        roughness = texture(material.roughness.texture, texCoords).r;
    }
    else
        roughness = material.roughness.color.r;

    float ao;
    if(material.ao.isTexture){
        ao = texture(material.ao.texture, texCoords).r;
    }
    else
        ao = material.ao.color.r;

    // Input lighting data
    vec3 N = normal;
    vec3 V = viewDir;
    vec3 R = reflect(-V, N);

    // Calculate reflectance at normal incidence
    vec3 F0 = vec3(0.04);
    F0 = mix(F0, albedo, metallic);

    // Reflectance equation
    vec3 Lo = vec3(0.0);
    for(int i = 0; i < 1; ++i)
    {
        // Calculate per-light radiance
        vec3 L = lightDir;
        vec3 H = normalize(V + L);
        float distance = length(lightDir);
        float attenuation = 1.0 / (distance * distance);
        vec3 radiance = lightColor * attenuation;

        // Cook-Torrance BRDF
        float NDF = DistributionGGX(N, H, roughness);
        float G   = GeometrySmith(N, V, L, roughness);
        vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);

        vec3 numerator    = NDF * G * F;
        float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.0001; // + 0.0001 to prevent divide by zero
        vec3 specular = numerator / denominator;

         // kS is equal to Fresnel
        vec3 kS = F;

        vec3 kD = vec3(1.0) - kS;

        kD *= 1.0 - metallic;

        // Scale light by NdotL
        float NdotL = max(dot(N, L), 0.0);

        // Adding that to outgoing radiance Lo
        Lo += (kD * albedo / PI + specular) * radiance * NdotL;
    }

    // Ambient lighting (we now use IBL as the ambient term)
    vec3 F = fresnelSchlickRoughness(max(dot(N, V), 0.0), F0, roughness);

    vec3 kS = F;
    vec3 kD = 1.0 - kS;
    kD *= 1.0 - metallic;

    vec3 irradiance = texture(irradianceMap, N).rgb;
    vec3 diffuse      = irradiance * albedo;

    // Smple both the pre-filter map and the BRDF lut and combine them together to get the IBL specular part.
    const float MAX_REFLECTION_LOD = 4.0;
    vec3 prefilteredColor = textureLod(prefilterMap, R,  roughness * MAX_REFLECTION_LOD).rgb;
    vec2 brdf  = texture(brdfLUT, vec2(max(dot(N, V), 0.0), roughness)).rg;
    vec3 specular = prefilteredColor * (F * brdf.x + brdf.y);

    vec3 ambient = (kD * diffuse + specular) * ao;

    vec3 color = ambient + Lo;

    // HDR tonemapping
    color = color / (color + vec3(1.0));
    // Gamma correction
    color = pow(color, vec3(1.0/2.2));

    FragColor = vec4(color , 1.0);
}

