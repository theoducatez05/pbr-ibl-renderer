#version 330 core
out vec4 FragColor;
in vec3 WorldPos;

// The equirectangular HDR map input
uniform sampler2D equirectangularMap;

const vec2 invAtan = vec2(0.1591, 0.3183);

//--------------------------------------
// Treating the HDR flat map as it was a sphere to sample it
vec2 SampleSphericalMap(vec3 v)
{
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= invAtan;
    uv += 0.5;
    return uv;
}

void main()
{
    // Transform the UV coordinates as it was a sphere
    vec2 uv = SampleSphericalMap(normalize(WorldPos));

    // Sample with the UV calculated
    vec3 color = texture(equirectangularMap, uv).rgb;

    FragColor = vec4(color, 1.0);
}

