A program that allows you to import an 3D object and visualise it with PBR (Physically Based Rendering) textures and IBL (Image Based Lighting).

Made in C++ OpenGL GLSL and ImGui. Made as part of my final project in Realtime Rendering class.

The current features are:

- Import a 3D model (.fbx, .obj, .dae)
- Import PBR textures to its material (albedo, normal, roughness, metallic, AO, height)
- Flip and activate textures
- Set default PBR material values if no texture
- Change light properties
- Change the IBL skybox

Here are some screenshots of the program:

The main interface with an object and an IBL cubemap skybox
![The main interface image](images/screen1.jpg)

With another object with another skybox
![The main interface image](images/screen2.jpg)

With another object and with the irradiance skybox of one of the IBL skybox cubemap
![The main interface image](images/screen3.jpg)